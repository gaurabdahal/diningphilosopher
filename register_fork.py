import socket
import constants
import pickle

def deserialize(self,data):
    return pickle.loads(data)

def serialize(self,data):
    return pickle.dumps(data,protocol=2)

try:
    msocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    msocket.sendto(serialize(["fork",constants.FORK_PORT]),(constants.MONITOR_IP,constants.MONITOR_PORT))
except socket.error as e:
    print("Connection error 0: "+str(e))
try:
    #this is test
    data, server =msocket.recvfrom(constants.DATA_SIZE)
    id = deserialize(data)
    print(id)
except socket.error as e:
    print("Connection error 1: "+str(e))



