import pickle
import socket

#------------------------------ Variables -------------------------------
forks = [] #just a placeholder. Its values are set from other files

#------------------------------ Constants -------------------------------
FORK_PORT = 6541
MONITOR_IP = socket.gethostbyname("sigma27")
DATA_SIZE = 2048
MONITOR_PORT = 6444
MONITOR_PORT2 = 6555

STATE_THINKING = 0
STATE_WAITING = 1
STATE_EATING = 2

STATE_THINKING_STRING = "Thinking"
STATE_WAITING_STRING = "Waiting"
STATE_EATING_STRING = "Eating"

CURRENT_TIME_STRING = "Current Time"
PHILOSOPHER_HEADER_STRING = "Philo# "
DISPLAY_TIME_FORMAT = "%B %d %H:%M:%S" #month_short day hour:minute:second
DISPLAY_COLUMN_OFFSET = 20
DISPLAY_COLUMN_SPACE = "{: <"+str(DISPLAY_COLUMN_OFFSET)+"s}"


ACTION_PICK_FORK = 1
ACTION_DROP_FORK = 0

ACTION_SUCCESS = 1
ACTION_FAIL = 0

#-------------------------------- Helper methods --------------------------
def fork_count():
    return len(forks)


def get_fork(index):
    return forks[index]


def get_fork1(index):
    fork = get_fork(index % len(forks))
    return fork if fork else get_fork(len(forks)-1)
    # if fork:
    #     return fork
    # else:
    #     return get_fork(len(forks) - 1)


def get_fork2(index):
    fork = get_fork((index + 1) % len(forks))
    return fork if fork else get_fork(len(forks) - 1)
    # if fork:
    #     return fork
    # else:
    #     return get_fork(len(forks) - 1)

def unserialize(data):
    return pickle.loads(data)

def serialize(data):
    return pickle.dumps(data,protocol=2)


#
# def get_all_forks():
#     global forks
#     port = 4001
#     try:
#         msocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#         msocket.sendto(serialize(["philosopher",port]),(constants.MONITOR_IP,constants.MONITOR_PORT2))
#     except socket.error as e:
#         print("Connection error 0: "+str(e))
#     try:
#         #this is test
#         data, server = msocket.recvfrom(2048)
#         forks = unserialize(data)
#         print(forks)
#     except socket.error as e:
#         print("Connection error 1: "+str(e))
#
# if __name__ == "__main__":
#     get_all_forks()
