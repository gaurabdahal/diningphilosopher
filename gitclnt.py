import socket
import pickle
from threading import Thread
import time
import git


class Fork:

	def __init__(self):
		try:
			self.msocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		except socket.error as e:
			print("Connection error 0: "+str(e))

		try:
			self.msocket.connect(("192.168.0.3",4999))
			t1 = Thread(target=self.runinbg,args=(5,))
			t1.start()
		except socket.error as e:
			print("Connection error 1: "+str(e))


    #Consider each fork as a thread. It will run in background
	def runinbg(self,data):
		while True:
			time.sleep(3)
			data = self.msocket.recv(1024)
			self.id = self.deserialize(data)
			if self.id == "q":
				self.isRunning = False
				print("quitting")
				print(self.id)
			else:
				print("Running git pull")
				g = git.Git("~/Desktop/dp")
				g.pull("origin","master")
				
				#repo = git.Repo('https://gitlab.com/gaurabdahal/diningphilosopher.git')
				#o = repo.remotes.origin
				#o.pull()
				

    # Since this method is called from thread, it will run in background and keep
    # accepting data
	def recieve_data(self):
		pass

	def deserialize(self,data):
		return pickle.loads(data)


fork = Fork()
#fork.enable()


