import socket
import pickle
import random
from threading import Thread
import time
import multiprocessing
import constants
from connection import *
from fork import Fork

forks=[]
timer=90
port = 6542
bg_processes=[]

class Philosopher:
    philosopher_id = None
    host = "localhost"
    fork_left = fork_right= None
    is_running = False


    def __init__(self,id,fork_left,fork_right):
        self.philosopher_id= id
        self.fork_left = fork_left
        self.fork_right = fork_right

    def come_to_life(self):
        self.is_running = True

        while self.is_running:
            #self.report_status(pickle.dumps([self.philosopher_id, 0]))  # IS THINKING
            time.sleep(random.randint(1,3)) #sleep 1 to 3 seconds
            self.tryToEat()
        pass

    def tryToEat(self):
        fork_l, fork_r = self.fork_left, self.fork_right
        self.print_status(serialize([self.philosopher_id, constants.STATE_WAITING]))  # IS WAITING
        client = None
        client2 = None
        while self.is_running:
            print("isrunning.. getting left fork")
            client = self.get_left_fork(fork_l)  # MUST get the left fork
            is_available = self.get_right_fork(fork_r)  # TRY to get the right fork

            #is_available = [acquired, client]
            #acquired -> True/False



            client2 = is_available[1]
            if not is_available[0]:  # If left fork not acquired
                self.release_fork(client)  # Release the left fork
                fork_l, fork_r = fork_r, fork_l  # Swap the forks, so that next time you acquire the right fork first
            else:
                break

        if self.is_running:
            self.dining()
            # Release both forks after done eating
            self.release_fork(client)
            self.release_fork(client2)
        else:
            if client:
                client.close()
            if client2:
                client2.close()

            if self.is_running:
                time.sleep(random.randint(3, 6))



    def get_left_fork(self, fork):
        while self.is_running:
            print("trying to get left fork")
            try:
                time.sleep(0.2)
                client = TCPClient(fork[0], fork[1])
                client.connect()
                print("fork="+fork)
                client.send(serialize([self.philosopher_id, constants.ACTION_PICK_FORK]))
                data = client.receive()
                print("data="+data)
                if data:
                    print(" deserialized data="+data)
                    response = unserialize(data)
                    print("response"+response)
                    # response -> [fork_id, philosopher_id, action, success]
                    # action   -> 1 = acquiring the fork, 0 = releasing the fork
                    # success  -> 1 = successful, 0 = failure
                    if response[1] == self.philosopher_id and response[2] == constants.ACTION_PICK_FORK and response[3] == constants.ACTION_SUCCESS:
                        return client
                client.close()
            except Exception as e:
                self.is_running=False #TODO uncomment this line
                print("Error getting left fork :"+str(e))
                pass


    # Doesn't block the execution if client cannot acquire the fork
    def get_right_fork(self, fork):
        client = TCPClient()
        client.connect(fork[0], fork[1])
        client.send(pickle.dumps([self.philosopher_id, 1]))
        data = client.receive()
        if data:
            response = pickle.loads(data)
            # response -> [fork_id, philosopher_id, action, success]
            # action   -> 1 = acquiring the fork, 0 = releasing the fork
            # success  -> 1 = successful, 0 = failure
            if response[1] == self.philosopher_id and response[2] == 1 and response[3] == 1:
                return [True, client]
        client.close()
        return [False, client]

    def release_fork(self, client):
        client.send(pickle.dumps([self.id, 0]))  # Sends [id, 0] to server. 0 being command for releasing the fork
        data = client.receive()
        if data:
            response = pickle.loads(data)
            # response -> [fork_id, philosopher_id, action, success]
            # action   -> 1 = acquiring the fork, 0 = releasing the fork
            # success  -> 1 = successful, 0 = failure
            if response[1] == self.id and response[2] == 0 and response[3] == 1:
                client.close()
            else:
                pass

    def print_status(self,data):
        print(unserialize(data))
#----------------------END Philosopher()---------------------------------------------------

def communicate_with_fork(identifier, fork_left, fork_right):
        philosopher = Philosopher(identifier, fork_left, fork_right)
        philosopher.come_to_life()
        pass

def new_forks(index):
    fork = Fork(forks[index][0],forks[index][1])


def create_forks():
    for i in range(len(forks)):
        fork_process = multiprocessing.Process(target=new_forks,
                                              args=(i,))
        #philosophers_processes.append(philosopher)
        fork_process.start()
    pass
def create_philosophers():
    global bg_processes
    print("creating philosopher")
     # Create Philosophers (Client Processes)
    print(len(forks))
    for i in range(len(forks)):
        philosopher_process = multiprocessing.Process(target=communicate_with_fork,
                                              args=(i, left_fork(i), right_fork(i),))
        bg_processes.append(philosopher_process)
        philosopher_process.start()

    time.sleep(30)
    print("Executed "+str(timer)+" seconds. Now terminating running processes")
    #terminate()  # Terminate all child processes



def get_forks_list():
    global forks
    try:
        msocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        msocket.sendto(serialize(["philosopher",port]),(constants.MONITOR_IP,constants.MONITOR_PORT))
    except socket.error as e:
        print("Connection error 0: "+str(e))
    try:
        #this is test
        data, server = msocket.recvfrom(2048)
        forks = unserialize(data)
        print(forks)
    except socket.error as e:
        print("Connection error 1: "+str(e))


def left_fork(index):
    global forks
    fork = forks[index % len(forks)]
    if fork:
        return fork
    else:
        return forks[len(forks) - 1]


def right_fork(index):
    global forks
    fork = forks[(index + 1) % len(forks)]
    if fork:
        return fork
    else:
        return forks(len(forks) - 1)


def unserialize(data):
        return pickle.loads(data)

def serialize(data):
    return pickle.dumps(data,protocol=2)


if __name__ == "__main__":
    try:
        get_forks_list()
        create_philosophers()
    except KeyboardInterrupt as e:
        for i in bg_processes:
            i.terminate()


