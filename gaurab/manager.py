import multiprocessing
import values
import constants
from connection import Display
import datetime
import socket
from threading import Thread
import pickle

class Display1:
    def __init__(self, host=constants.MONITOR_IP, port=constants.MONITOR_PORT):
        self.host, self.port = host, port
        self.run = False
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        except socket.error as exp:
            print('Failed to create server UDP socket object!!', exp)
        try:
            self.socket.bind((self.host, self.port))
        except socket.error as exp:
            print('Cannot bind server to ' + str(self.host) + ' on port ' + str(self.port), exp)

    def print_header(self):
        self.num = len(values.forks)
        self.headers = ['Current Time']
        self.divider_line = '----------------------'
        self.display_format = '{: >20} '
        self.initialstate = [datetime.datetime.now().strftime('%B %d %H:%M:%S')]
        for i in range(self.num):
            self.headers.append("Philo# " + str(i))
            self.initialstate.append("Thinking")
            if i != (self.num - 1):
                self.display_format += "{: >20} "
            else:
                self.display_format += "{: >20}"
            self.divider_line += "----------------------"

        print(self.display_format.format(*self.headers))
        print(self.divider_line)
        print(self.display_format.format(*self.initialstate))


    def start(self):
        self.run = True
        while self.run:
            try:
                d = self.receive()
                Thread(target=self.handle_data, args=(d,)).start()
            except Exception as e:
                print("Exception :",e)

    def stop(self):
        self.run = False

    def send(self, data, remote_address):
        self.socket.sendto(data, remote_address)

    def receive(self, size=1024):
        try:
            data = self.socket.recvfrom(size)
            return data
        except Exception as e:
            print("receive exception ",e)

    def close(self):
        self.socket.close()


    def handle_data(self, data):
        state = []

        response = pickle.loads(data[0])
        process_id = response[0]
        status_type = response[1]

        status = ""
        if status_type == 0:
            status = "Thinking"
        elif status_type == 1:
            status = "Waiting"
        elif status_type == 2:
            status = "Eating"

        current_time = datetime.datetime.now().strftime('%B %d %H:%M:%S')
        state.append(current_time)

        for i in range(self.num):
            if i == process_id:
                state.append(status)
            else:
                state.append("--------")

        print(self.display_format.format(*state))


def run_in_background(length):
    try:
        display_module = Display1(constants.MONITOR_IP,constants.MONITOR_PORT)
        #display_module = Display(5)
        display_module.print_header()
        display_module.start()
    except Exception as e:
        print("run_in_background :",str(e))


def display():
    display_ = multiprocessing.Process(target=run_in_background, args=(5,))
    display_.start()


if __name__ == "__main__":
    display()
