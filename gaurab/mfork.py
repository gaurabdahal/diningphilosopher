import socket
import pickle
from threading import Thread
import time
import constants
import multiprocessing
import values

class Fork:

    def __init__(self,index,host,port):
        self.id=index
        self.is_dirty=False
        self.host=host
        self.port = port
        self.is_running=True


    #Consider each fork as a thread. It will run in background
    def enable(self):
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error as exp:
            print("Connection establishment problem " + str(exp))

        try:
            self.socket.bind((self.host, self.port))
        except socket.error as exp:
            print("socket bind problem " + str(exp))

        self.socket.listen(0)
        while self.is_running:
            print("accepting data")
            self.server,self.host = self.socket.accept()
            Thread(target = self.receive_data,args=(self.server,)).start()


    def receive_data(self,connection):
            while self.is_running:
               # time.sleep(0.1)
                try:
                    data = connection.recv(constants.DATA_SIZE)
                    print("data="+str(pickle.loads(data)))
                    if data:
                        philosopher_request = values.unserialize(data)
                        print(" deserialized data="+str(philosopher_request))
                        if philosopher_request[1] == constants.ACTION_PICK_FORK:
                            print("philosopher trying to get fork")
                            if self.is_dirty:
                                connection.send(values.serialize([self.id, philosopher_request[0], constants.ACTION_PICK_FORK, constants.ACTION_FAIL]))
                                print("data sent to philosopher")
                                break
                            else:  # If fork is clean/not being used
                                self.is_dirty = True
                                self.current_philosopher = philosopher_request[0]
                                connection.send(self.serialize([self.id, self.current_philosopher, constants.ACTION_PICK_FORK, constants.ACTION_SUCCESS]))
                                print("data sent to philosopher else")

                        elif philosopher_request[1] == 0:
                            # Fork has to be dirty/being used
                            # Release request can only be made by the philosopher holding the fork
                            if self.is_dirty and self.current_philosopher == philosopher_request[0]:
                                last_user = self.current_philosopher
                                self.is_dirty = False
                                self.current_philosopher = None
                                # Send [fork_id, philosopher_id, action, success]
                                # action  = 0 for releasing fork
                                # success = 1 for success
                                connection.send(pickle.dumps([self.id, last_user, 0, 1]))
                                break
                            else:
                                # Send [fork_id, philosopher_id, action, success]
                                # action  = 0 for releasing fork
                                # success = 0 for failure
                                connection.send(pickle.dumps([self.id, philosopher_request[0], 0, 0]))
                                break

                except socket.error as e:
                    print("error getting data : " + str(e))
                    #connection.close()
            connection.close()


    def disable(self):
        self.is_running = False


    def deserialize(self,data):
        return pickle.loads(data)

    def serialize(self,data):
        return pickle.dumps(data)

#--------------End Class --------------------------



background_forks = []
def create_forks():
    global background_forks

    for i in range(len(values.forks)):
        fork = Fork(i,values.forks[i][0],values.forks[i][1])
        process = multiprocessing.Process(target=fork.enable,args=())
        background_forks.append(process)
        process.start()

if __name__ == "__main__":
    try:
        create_forks()
    except KeyboardInterrupt:
        for f in background_forks:
            f.terminate()



