import values
import time
import constants
import random
import multiprocessing
import socket
import pickle
from connection import UDPClient
from connection import Display
from connection import TCPClient

class Philosopher:

    def __init__(self,id,fork_left,fork_right):
        self.philosopher_id= id
        self.left_fork = fork_left
        self.right_fork = fork_right


    def enable(self):
        self.is_running = True
        while self.is_running:
            self.display(values.serialize([self.philosopher_id, constants.STATE_THINKING]))
            time.sleep(random.randint(1, 3))
            self.sitOnTable()


    def sitOnTable(self):
        forkl,forkr = self.left_fork,self.right_fork
        while self.is_running:
            self.display(values.serialize([self.philosopher_id, constants.STATE_WAITING]))
            fork1 = self.get_left_fork(forkl)
            is_available = self.get_right_fork(forkr)

            fork2 = is_available[1]
            if not is_available[0]:  # If left fork not acquired
                self.release_fork(fork1)  # Release the left fork
                forkl, forkr = forkr, forkl  # Swap the forks, so that next time you acquire the right fork first
            else:
                break

        if self.is_running:
            self.dining()
            # Release both forks after done eating
            self.release_fork(fork1)
            self.release_fork(fork2)
        else:
            if fork1:
                fork1.close()
            if fork2:
                fork2.close()

    def dining(self):
        self.display(pickle.dumps([self.philosopher_id, 2]))  # IS EATING
        time.sleep(random.randint(3, 6))

    def release_fork(self, client):
        client.send(pickle.dumps([self.philosopher_id, 0]))  # Sends [id, 0] to server. 0 being command for releasing the fork
        data = client.receive()
        if data:
            response = pickle.loads(data)
            # response -> [fork_id, philosopher_id, action, success]
            # action   -> 1 = acquiring the fork, 0 = releasing the fork
            # success  -> 1 = successful, 0 = failure
            if response[1] == self.philosopher_id and response[2] == 0 and response[3] == 1:
                client.close()
            else:
                pass

    # Blocks the execution until the client acquires the fork
    def acquire_blocking(self, fork):
        while self.run:
            try:
                time.sleep(0.2)
                client = TCPClient()
                client.connect(fork[0], fork[1])
                client.send(pickle.dumps([self.philosopher_id, 1]))
                data = client.receive()
                if data:
                    response = pickle.loads(data)
                    # response -> [fork_id, philosopher_id, action, success]
                    # action   -> 1 = acquiring the fork, 0 = releasing the fork
                    # success  -> 1 = successful, 0 = failure
                    if response[1] == self.philosopher_id and response[2] == 1 and response[3] == 1:
                        return client
                client.close()
            except:
                pass


    def get_left_fork(self,forkl):
        while self.is_running:
            try:
                time.sleep(2)
                try:
                    lfork = TCPClient(forkl[0],forkl[1])
                    lfork.connect()
                except socket.error as e:
                    print("socekt error lf =",e)
                lfork.send(values.serialize([self.philosopher_id, constants.ACTION_PICK_FORK]))
                data = lfork.receive(1024)
                print(str(values.unserialize(data)))
                if data:
                    response = values.unserialize(data)
                    #print(str(response))
                    # response -> [fork_id, philosopher_id, action, success]
                    if response[1] == self.philosopher_id and response[2] == constants.ACTION_PICK_FORK and response[3] == constants.ACTION_SUCCESS:
                        return lfork
                lfork.close()
            except Exception as e:
                #print("Exception lf= ",e)
                pass


    def get_right_fork(self, fork):
        client = TCPClient(fork[0], fork[1])
        client.connect()
        client.send(pickle.dumps([self.philosopher_id, 1]))
        data = client.receive()
        if data:
            response = pickle.loads(data)
            # response -> [fork_id, philosopher_id, action, success]
            # action   -> 1 = acquiring the fork, 0 = releasing the fork
            # success  -> 1 = successful, 0 = failure
            if response[1] == self.philosopher_id and response[2] == 1 and response[3] == 1:
                return [True, client]
        client.close()
        return [False, client]


    @staticmethod
    def display(msg):
        client = UDPClient()
        client.send(constants.MONITOR_IP, constants.MONITOR_PORT, msg)
        client.close()


#---------------------------- END CLASS ---------------------------------


background_philosophers = []

def philosopher_process(identifier, fork_left, fork_right):
    philosopher = Philosopher(identifier, fork_left, fork_right)
    philosopher.enable()


def create_philosophers():
    global background_philosophers

    for i in range(len(values.forks)):
        process = multiprocessing.Process(target=philosopher_process,args=(i,values.get_fork_left(i),values.get_fork_right(i),))
        background_philosophers.append(process)
        process.start()

    time.sleep(100)
    for f in background_philosophers:
            f.terminate()




if __name__ == "__main__":
    try:
        create_philosophers()
    except KeyboardInterrupt:
        for f in background_philosophers:
            f.terminate()
