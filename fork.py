import socket
import pickle
from threading import Thread
import time
import constants
import multiprocessing

class Fork:
    id = None
    host = "localhost"
    port = 4446
    is_running = True
    is_dirty = False #Initially no philosopher are using the fork
    current_philosopher = None

    def __init__(self):
        try:
            self.msocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.msocket.sendto(self.serialize(["fork",self.port]),(constants.MONITOR_IP,constants.MONITOR_PORT))
        except socket.error as e:
            print("Connection error 0: "+str(e))
        try:
            #this is test
            data, server = self.msocket.recvfrom(constants.DATA_SIZE)
            self.id = self.deserialize(data)
            print(self.id)
        except socket.error as e:
            print("Connection error 1: "+str(e))
        finally:
            self.msocket.close()



    #Consider each fork as a thread. It will run in background
    def enable(self):
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error as exp:
            print("Connection establishment problem " + str(exp))

        try:
            self.socket.bind((self.host, self.port))
        except socket.error as exp:
            print("socket bind problem " + str(exp))

        self.socket.listen(0)
        while self.is_running:
            print("accepting data")
            (self.server,(self.ip,self.port)) = self.socket.accept()
            print("data accepted")
            mthread = Thread(target = self.recieve_data,args=(self.server,))
            mthread.start()


    def disable(self):
        self.is_running = False


    # Since this method is called from thread, it will run in background and keep
    # accepting data
    def recieve_data(self, connection):
        print("receiving data")
        while self.is_running:
            try:
                data = connection.recv(constants.DATA_SIZE)
                print("data="+data)
                if data:
                    print(" deserialized data="+data)
                    request = self.deserialize(data)  # request -> [philosopher_id, action]
                    # If the request action is acquiring the fork
                    if request[1] == constants.ACTION_PICK_FORK:
                        if self.is_dirty:
                            connection.send(self.serialize([self.id, request[0], constants.ACTION_PICK_FORK, constants.ACTION_FAIL]))
                            break
                        else:  # If fork is clean/not being used
                            self.is_dirty = True
                            self.current_philosopher = request[0]

                            connection.send(self.serialize([self.id, self.current_philosopher, constants.ACTION_PICK_FORK, constants.ACTION_SUCCESS]))
                    # If the request action is releasing the fork
                    elif request[1] == constants.ACTION_DROP_FORK:
                        # Fork has to be dirty/being used
                        # Release request can only be made by the philosopher holding the fork
                        if self.is_dirty and self.current_philosopher == request[0]:
                            last_user = self.current_philosopher
                            self.is_dirty = False
                            self.current_philosopher = None
                            connection.send(self.serialize([self.id, self.current_philosopher, constants.ACTION_DROP_FORK, constants.ACTION_SUCCESS]))
                            break
                        else:
                            connection.send(self.serialize([self.id, request[0], constants.ACTION_DROP_FORK, constants.ACTION_FAIL]))
                            break
            except:
                break
        connection.close()

		
    def deserialize(self,data):
        return pickle.loads(data)

    def serialize(self,data):
        return pickle.dumps(data)




process=None
fork=None

if __name__ == "__main__":
    try:
        fork = Fork()
        process = multiprocessing.Process(target=fork.enable,args=())
        process.start()

        # time.sleep(130)
        # print(str(fork.id)+" quitting")
        # fork.disable()
    except KeyboardInterrupt:
        fork.disable()
        process.terminate()



